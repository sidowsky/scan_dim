package com.skaner;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Arrays;

import static org.opencv.imgproc.Imgproc.THRESH_BINARY;
import static org.opencv.imgproc.Imgproc.threshold;

public class Main {

    private static final String SCAN_1 = "resources/SCAN1";
    private static final String SCAN_2 = "resources/SCAN2";
    private static final String SCAN_3 = "resources/SCAN3";
    private static final String SCAN_4 = "resources/SCAN4";
    private static final String SCAN_5 = "resources/SCAN5";

    private static final String RESULT = "resources/RESULTS";
    private static final String O_1 = "O1";
    private static final String O_2 = "O2";
    private static final String O_3 = "O3";
    private static final String O_4 = "O4";
    private static final String O_5 = "O5";


    private static final String OUTPUT_DIRECTORY_1 = "resources/OUTPUT1/";
    private static final String OUTPUT_DIRECTORY_2 = "resources/OUTPUT2/";
    private static final String OUTPUT_DIRECTORY_3 = "resources/OUTPUT3/";
    private static final String OUTPUT_DIRECTORY_4 = "resources/OUTPUT4/";

    private static final String OUTPUT_DIRECTORY_5 = "resources/OUTPUT5/";
    private static final String OUT = "out.txt";
    private static final String OUT_SCALED = "out_scaled.txt";

    private static final String COMMON = "scan.txt";
    private static final double[] WHITE = new double[]{255.0, 255.0, 255.0};
    private static final int DEFAULT_IMAGE_WIDTH = 640;

    private static final ArrayList<String> EMPTY_LIST = new ArrayList<String>() {
    };

    private static int FROM_LEFT = 0;
    private static int FROM_RIGHT = 1;


    public static void main(String[] args) {

        final long startTime = System.currentTimeMillis();
        clearAllFiles();


        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);

        Mat matrix = Mat.eye(3, 3, CvType.CV_8UC1);
        //   System.out.println("mat "+ matrix.dump());

        findEdgesInAllPictures();
        getFunctionValue();


        // proba wyleczenia odleglosci krawedz rurki od krawedzi zdjecia

        Mat outputPic = Imgcodecs.imread(OUTPUT_DIRECTORY_3 + "scan004.jpg");
        Mat bwPhoto = Imgcodecs.imread("resources/line.jpg");


        //   System.out.println(Arrays.toString(bwPhoto.get(179, 0)));
        //   System.out.println(Arrays.toString(bwPhoto.get(0, 179)));


        System.out.println("a teraz obrazek po detekcji");

        ArrayList<String> firstLeftWhitePixels = findFirstLeftWhitePixels(outputPic, 0, FROM_LEFT);

        // tests
        Mat mat = Imgcodecs.imread("D:\\REPOS\\PRACA_MAGISTERSKA\\resources\\OUTPUT3\\out_scaled.jpg");
        findFirstLeftWhitePixels(mat, 0, FROM_RIGHT);
        double[] doubles = mat.get(380, 311);

        // tests

        final long endTime = System.currentTimeMillis();
        System.out.println("Total execution time: " + (endTime - startTime) / 1000.0 + " seconds ");
    }

    /**
     * Metoda zwraca wszystkie pliki w danym katalogu
     *
     * @param directory
     */
    private static File[] listAllFilesFromDir(String directory) {
        File folder = new File(directory);
        return folder.listFiles(); // TODO: 2018-05-09 TABLICE NALEŻY POSORTOWAC!!!
    }

    /**
     * Metoda wykonuje rozpoznawanie obrazu na pliku
     */
    private static void doCanny(File file, String dir, int lowerTreshold, int upperTreshld) {  // 50 150
        Mat color = Imgcodecs.imread(file.getAbsolutePath());
        Mat gray = new Mat();
        Mat draw = new Mat();
        Mat wide = new Mat();

        Imgproc.GaussianBlur(color, color, new Size(3, 3), 5.0);

        Imgproc.cvtColor(color, gray, Imgproc.COLOR_BGR2GRAY);
        Imgproc.Canny(gray, wide, lowerTreshold, upperTreshld, 3, false);
        wide.convertTo(draw, CvType.CV_8U);
        if (Imgcodecs.imwrite(dir + file.getName(), draw)) {
            System.out.println(dir + file.getName() + " edges detected");
        }

        color.release();
        gray.release();
        draw.release();
        wide.release();
    }

    /**
     * Czyści wszystkie pliki tekstowe
     */
    private static void clearAllFiles() {
        File[] files = listAllFilesFromDir(RESULT);
        for (File f : files) {

            File[] filesToWipe = listAllFilesFromDir(f.getAbsolutePath());
            for (File ftw : filesToWipe) {
                try {
                    wipeAllData(EMPTY_LIST, ftw.getAbsolutePath());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * This method finds all edges
     */
    private static void findEdgesInAllPictures() {
        File[] photos1 = listAllFilesFromDir(SCAN_1);
        File[] photos2 = listAllFilesFromDir(SCAN_2);
        File[] photos3 = listAllFilesFromDir(SCAN_3);
        File[] photos4 = listAllFilesFromDir(SCAN_4);
        File[] photos5 = listAllFilesFromDir(SCAN_5);

        for (File p : photos1) {
            doCanny(p, OUTPUT_DIRECTORY_1, 50, 150);
        }
        for (File p : photos2) {
            doCanny(p, OUTPUT_DIRECTORY_2, 50, 150);
        }
        for (File p : photos3) {
            doCanny(p, OUTPUT_DIRECTORY_3, 50, 150);
        }
        for (File p : photos4) {
            doCanny(p, OUTPUT_DIRECTORY_4, 50, 150);
        }
        for (File p : photos5) {
            doCanny(p, OUTPUT_DIRECTORY_5, 50, 100);
        }
    }

    /**
     * This method get all output photo (after Canny alghoritm) and store to  txt file values of functiond
     * describing curvature of pipe
     */
    private static void getFunctionValue() {
        File[] output1 = listAllFilesFromDir(OUTPUT_DIRECTORY_1);
        File[] output2 = listAllFilesFromDir(OUTPUT_DIRECTORY_2);
        File[] output3 = listAllFilesFromDir(OUTPUT_DIRECTORY_3);
        File[] output4 = listAllFilesFromDir(OUTPUT_DIRECTORY_4);
        File[] output5 = listAllFilesFromDir(OUTPUT_DIRECTORY_5);


        for (File o : output1) {
            computeFunctionValueByPhoto(o, O_1, 250);
        }
        for (File o : output2) {
            computeFunctionValueByPhoto(o, O_2, 330);
        }
        for (File o : output3) {
            computeFunctionValueByPhoto(o, O_3, 280);
        }
        for (File o : output4) {
            computeFunctionValueByPhoto(o, O_4, 280);
        }
        for (File o : output5) {
            computeFunctionValueByPhoto(o, O_5, 140);
        }
    }

    private static void computeFunctionValueByPhoto(File o, String destination, int startColumnNumber) {
        ArrayList<String> firstLeftWhitePixels = findFirstLeftWhitePixels(Imgcodecs.imread(o.getAbsolutePath()), startColumnNumber, FROM_RIGHT);

        String surfix = null;
        if (o.getName().equals("out.jpg")) {
            surfix = OUT;
        }
        if (o.getName().contains("scaled")) {
            surfix = OUT_SCALED;
        }
        if (o.getName().contains("scan")) {
            surfix = COMMON;
        }

        try {
            writeToFile(firstLeftWhitePixels, RESULT + "\\" + destination + "\\" + surfix);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * This method fins and store indexes of all first white pixels from given side side
     *
     * @param input             given photo to compute
     * @param startColumnNumber column from for loop stats
     * @param startsFrom        flag descriging from with side alghoritm shoud return value
     *                          from right is
     * @return
     */
    private static ArrayList<String> findFirstLeftWhitePixels(Mat input, int startColumnNumber, int startsFrom) {

        threshold(input, input, 25.0, 255.0, THRESH_BINARY);

        ArrayList<String> pixels = new ArrayList<>();
        for (int row = input.height(); row > 0; row--) {
            for (int col = startColumnNumber; col < input.width(); col++) {
                boolean trueOrFalse = Arrays.equals(input.get(row, col), WHITE);
                double[] currentPixel = input.get(row, col);
                if (Arrays.equals(input.get(row, col), WHITE) /*checkColor(currentPixel, 250)*/) {  // Arrays.equals(input.get(row, col), WHITE)     checkColor(input.get(row, col), 250)
                    if (startsFrom == FROM_LEFT) {
                        pixels.add(String.valueOf(col));
                        break;
                    } else {
                        pixels.add(String.valueOf(calculateRealFValue(col)));
                        break;
                    }

                }
            }
        }
        return pixels;
    }

    public static void writeToFile(ArrayList<String> pixels, String direcotry) throws IOException {
        Path file = Paths.get(direcotry);
        Files.write(file, pixels, Charset.forName("UTF-8"), StandardOpenOption.APPEND);
    }

    private static void wipeAllData(ArrayList<String> pixels, String direcotry) throws IOException {
        Path file = Paths.get(direcotry);
        Files.write(file, pixels, Charset.forName("UTF-8"));
    }

    /**
     * Zwraca konkretną wartość piksela (licząc z lewej strony)
     *
     */
    private static int calculateRealFValue(int index) {
        return DEFAULT_IMAGE_WIDTH - index;
    }

    private static boolean checkColor(double[] pixelColor, double rgbValue) {
        return (pixelColor[0] > rgbValue && pixelColor[2] > rgbValue && pixelColor[2] > rgbValue);
    }
}
